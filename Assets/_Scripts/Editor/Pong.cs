using System;
using System.Collections;
using UnityEditor;
using UnityEngine;
using UnityEngine.UIElements;
using Random = UnityEngine.Random;

public class Pong : EditorWindow
{
    [SerializeField] private VisualTreeAsset tree;
    [SerializeField] private float playerSpeed = 6f;
    private static Vector2 playerSize = new Vector2(20, 75);

    public VisualElement main;

    public static VisualElement leftPlayerBody;
    private static float leftPlayerPosition;
    
    public static VisualElement ballBody;
    private static Vector2 ballPosition;
    private static Vector2 ballVelocity;
    private static float ballRadius;
    
    public static VisualElement rightPlayerBody;
    private static float rightPlayerPosition;
    
    public static Label leftPlayerScore;
    
    public static Label rightPlayerScore;

    private static Pong window;
    private static Rect PongRect => window.position;
    private static float RightEdge => PongRect.xMax * .78f;
    private static float BottomEdge => PongRect.yMax * .78f;
    
    [MenuItem("Window/Pong")]
    public static void CreateWindow()
    {
        if (window == null)
        {
            window = GetWindow<Pong>();
        }
        
        window.titleContent = new GUIContent("Pong");
    }

    void OnEnable()
    {
        if (window == null)
        {
            window = GetWindow<Pong>();
        }
        
        tree.CloneTree(rootVisualElement);
        InitFields();
        ballRadius = ballBody.style.width.value.value * .78f;
        minSize = new Vector2(500, 400);
        
        ResetPong();
        
        // Start doing game loop
        EditorApplication.update += UpdateBallPosition;
    }

    void OnDisable()
    {
        // Stop doing game loop
        EditorApplication.update -= UpdateBallPosition;
    }

    private void OnGUI()
    { 
        // Queue next game loop
        main.MarkDirtyRepaint();
    }

    private void InitFields()
    {
        main = rootVisualElement.Q<VisualElement>("Main");
        
        leftPlayerBody =
            rootVisualElement.Q<VisualElement>("LeftPlayerBody");
        ballBody = 
            rootVisualElement.Q<VisualElement>("BallBody");
        rightPlayerBody = rootVisualElement.Q<VisualElement>("RightPlayerBody");
        
        leftPlayerScore = rootVisualElement.Q<Label>("LeftPlayerScore");
        rightPlayerScore = rootVisualElement.Q<Label>("RightPlayerScore");
    }

    [MenuItem("Tools/Pong/Reset")]
    private static void ResetPong()
    {
        // Set positions
        leftPlayerPosition = rightPlayerPosition = BottomEdge / 2 + 20;
        SetLeftPlayerPosition();
        SetRightPlayerPosition();
        
        // Set ball position
        ballPosition = new(RightEdge / 2,
            BottomEdge / 2);
        SetBallPosition();
        
        // Set ball velocity
        ballVelocity = new Vector2(Random.value - .5f, (Random.value - .5f) / 10);
        ballVelocity.Normalize();
        ballVelocity *= 6;
    }

    [MenuItem("Tools/Pong/Reset", true)]
    private static bool ValidateResetPong()
    {
        return window != null;
    }

    #region LeftPlayer
    
    [MenuItem("Tools/Pong/Move Left Pong Player Up &w")]
    private static void MoveLeftPlayerUp()
    {
        leftPlayerPosition -= window.playerSpeed;
        leftPlayerPosition = Mathf.Clamp(leftPlayerPosition, 0, BottomEdge);
        
        SetLeftPlayerPosition();
    }
    
    [MenuItem("Tools/Pong/Move Left Pong Player Down &s")]
    private static void MoveLeftPlayerDown()
    {
        leftPlayerPosition += window.playerSpeed;
        leftPlayerPosition = Mathf.Clamp(leftPlayerPosition, 0, BottomEdge);
        
        SetLeftPlayerPosition();
    }

    private static void SetLeftPlayerPosition()
    {
        leftPlayerBody.style.top = leftPlayerPosition;
        leftPlayerBody.MarkDirtyRepaint();
    }

    #endregion
    
    #region RightPlayer
    
    [MenuItem("Tools/Pong/Move Right Pong Player Up %UP")]
    private static void MoveRightPlayerUp()
    {
        rightPlayerPosition -= window.playerSpeed;
        rightPlayerPosition = Mathf.Clamp(rightPlayerPosition, 0, BottomEdge);
        
        SetRightPlayerPosition();
    }
    
    [MenuItem("Tools/Pong/Move Right Pong Player Down %DOWN")]
    private static void MoveRightPlayerDown()
    {
        rightPlayerPosition += window.playerSpeed;
        rightPlayerPosition = Mathf.Clamp(rightPlayerPosition, 0, BottomEdge);

        SetRightPlayerPosition();
    }
    
    private static void SetRightPlayerPosition()
    {
        rightPlayerBody.style.top = rightPlayerPosition;
        rightPlayerBody.MarkDirtyRepaint();
    }

    #endregion

    #region Ball

    private static void SetBallPosition()
    {
        // Apply position to ball
        ballBody.style.top = ballPosition.y;
        ballBody.style.left = ballPosition.x;
        
        ballBody.MarkDirtyRepaint();
    }

    private static void ResetBallPosition()
    {
        ballPosition = new(RightEdge / 2,
            BottomEdge / 2);
        SetBallPosition();
    }

    private static void UpdateBallPosition()
    {
        // Detect player collisions and update velocity
        bool pColl = CalculatePlayerCollisions();
        
        // Detect edge collisions and update velocity
        if (!pColl) // Only if player collision not detected
        {
            CalculateEdgeCollisions();
        }
        
        // Apply velocity to position
        ballPosition += ballVelocity / 12f;

        SetBallPosition();
    }

    private static bool CalculatePlayerCollisions()
    {
        // If collision with left player body
        if (ballPosition.x + ballRadius < ballBody.style.width.value.value &&
            Mathf.Abs(leftPlayerPosition - ballPosition.y) <
            ballBody.style.height.value.value)
        {
            ballVelocity.x *= -1.1f;
            
            Debug.Log("COLL LEFT PLAYER");
            
            return true;
        }

        // If collision with right player body
        if (ballPosition.x - ballRadius > RightEdge - ballBody.style.width.value.value &&
            Mathf.Abs(rightPlayerPosition - ballPosition.y) <
            ballBody.style.backgroundSize.value.x.value)
        {
            ballVelocity.x *= -1.1f;
            
            Debug.Log("COLL RIGHT PLAYER");
            
            return true;
        }

        return false;
    }

    private static void CalculateEdgeCollisions()
    {
        // If collision with top edge
        if (ballPosition.y < ballRadius)
        {
            ballVelocity.y *= -1;
        }
        
        // If collision with right edge
        if (RightEdge - ballPosition.x < ballRadius)
        {
            // Score for left player
            leftPlayerScore.text = (int.Parse(leftPlayerScore.text) + 1).ToString();

            // Reset ball
            ResetBallPosition();
            ballVelocity = new Vector2(- 1, Random.Range(-1, 1));
        }
        
        // If collision with bottom edge
        if (BottomEdge - ballPosition.y < ballRadius)
        {
            ballVelocity.y *= -1;
        }
        
        // If collision with left edge
        if (ballPosition.x < ballRadius)
        {
            // Score for right player
            rightPlayerScore.text = (int.Parse(rightPlayerScore.text) + 1).ToString();
            
            // Reset ball
            ResetBallPosition();
            ballVelocity = new Vector2(1, Random.Range(-1, 1));
        }
    }
    
    #endregion
}